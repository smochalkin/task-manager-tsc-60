package ru.smochalkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractSystemListener;

@Component
public final class ExitListener extends AbstractSystemListener {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "exit";
    }

    @Override
    @NotNull
    public String description() {
        return "Close the program.";
    }

    @Override
    @EventListener(condition = "@exitListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.exit(0);
    }

}
