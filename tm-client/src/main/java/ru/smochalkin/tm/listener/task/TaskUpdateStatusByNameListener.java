package ru.smochalkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractTaskListener;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskUpdateStatusByNameListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String name() {
        return "task-status-update-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Update task status by name.";
    }

    @Override
    @EventListener(condition = "@taskUpdateStatusByNameListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusName = TerminalUtil.nextLine();
        taskEndpoint.changeTaskStatusByName(sessionService.getSession(), name, statusName);
    }

}
