package ru.smochalkin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.endpoint.UserDto;
import ru.smochalkin.tm.exception.entity.UserNotFoundException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;

@Component
public class UserInfoListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "user-info";
    }

    @Override
    @NotNull
    public String description() {
        return "Show user info.";
    }

    @Override
    @EventListener(condition = "@userInfoListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        @Nullable final UserDto user = userEndpoint.findUserBySession(sessionService.getSession());
        if (user == null) throw new UserNotFoundException();
        System.out.println("User id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Role: " + user.getRole());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Middle name: " + user.getMiddleName());
    }

}
