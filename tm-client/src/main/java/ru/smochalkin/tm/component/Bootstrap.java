package ru.smochalkin.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.api.service.ICommandService;
import ru.smochalkin.tm.api.service.ILogService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractSystemListener;
import ru.smochalkin.tm.util.SystemUtil;
import ru.smochalkin.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private AbstractSystemListener[] systemListeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    private void init() {
        initPID();
        fileScanner.init();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager-client.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        System.out.println("** WELCOME TO CLIENT TASK MANAGER **");
        init();
        parseArgs(args);
        process();
    }

    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable String command = null;
        for (@NotNull final AbstractSystemListener listener : systemListeners) {
            if (arg.equals(listener.arg())) command = listener.name();
        }
        if (command == null) return;
        publisher.publishEvent(new ConsoleEvent(command));
    }

    public void process() {
        logService.debug("process start...");
        @NotNull String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("Enter command: ");
            command = TerminalUtil.nextLine();
            try {
                parseCommand(command);
                logService.info("`" + command + "` command executed");
            } catch (Exception e) {
                logService.error(e);
            }
            System.out.println();
        }
    }

    public void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        publisher.publishEvent(new ConsoleEvent(cmd));
    }

}
