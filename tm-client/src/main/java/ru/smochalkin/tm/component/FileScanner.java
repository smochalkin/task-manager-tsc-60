package ru.smochalkin.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.listener.AbstractSystemListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class FileScanner implements Runnable {

    private final int interval = 3;

    @NotNull
    private final String path = "./";

    @NotNull
    @Autowired
    public Bootstrap bootstrap;

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private AbstractSystemListener[] systemListeners;

    public void init() {
        for (@NotNull final AbstractListener listener : systemListeners) {
            commands.add(listener.name());
        }
        es.scheduleWithFixedDelay(this, interval, interval, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        @NotNull final File file = new File(path);
        for (File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            if (!commands.contains(fileName)) continue;
            try {
                System.out.println("::FILE SCANNER START::");
                bootstrap.parseCommand(fileName);
            } catch (Exception e) {
                bootstrap.getLogService().info("File scanner info: " + e.getMessage());
            } finally {
                item.delete();
                System.out.println("::FILE SCANNER END::");
                System.out.println();
            }
        }
    }

}
