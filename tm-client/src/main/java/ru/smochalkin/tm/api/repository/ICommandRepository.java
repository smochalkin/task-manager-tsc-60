package ru.smochalkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.listener.AbstractSystemListener;

import java.util.Collection;
import java.util.List;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractListener> getCommands();

    @NotNull
    Collection<AbstractSystemListener> getArguments();

    @NotNull
    List<String> getCommandNames();

    @NotNull
    List<String> getCommandArgs();

    @Nullable
    AbstractListener getCommandByName(@NotNull String name);

    @Nullable
    AbstractListener getCommandByArg(@NotNull String arg);

    void add(@NotNull AbstractListener command);

}
