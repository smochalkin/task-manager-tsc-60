package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.dto.UserDto;

public interface IUserService extends IService<UserDto> {

    @Nullable
    UserDto findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

    void create(@Nullable String login, @Nullable String password, @Nullable String email);

    void setPassword(@Nullable String userId, @Nullable String password);

    void updateById(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    boolean isLogin(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
