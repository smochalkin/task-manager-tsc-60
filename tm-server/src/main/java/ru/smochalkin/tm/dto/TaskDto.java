package ru.smochalkin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.listener.JpaEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@EntityListeners(JpaEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDto extends AbstractBusinessEntityDto {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

}
