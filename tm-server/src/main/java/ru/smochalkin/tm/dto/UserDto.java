package ru.smochalkin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.listener.JpaEntityListener;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@EntityListeners(JpaEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDto extends AbstractEntityDto {

    @NotNull
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Column(columnDefinition="boolean DEFAULT false")
    private boolean lock = false;

    public boolean isLock() {
        return lock;
    }

}
