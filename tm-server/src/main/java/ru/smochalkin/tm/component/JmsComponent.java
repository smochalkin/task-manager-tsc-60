package ru.smochalkin.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.service.IJmsService;
import ru.smochalkin.tm.dto.LogDto;
import ru.smochalkin.tm.service.JmsSenderService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JmsComponent {

    private static final int THREAD_COUNT = 3;

    @Nullable
    private static JmsComponent instance;

    @NotNull
    private final IJmsService service = new JmsSenderService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public static JmsComponent getInstance() {
        if (instance == null)
            instance = new JmsComponent();
        return instance;
    }

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final LogDto logDto = service.createMessage(object, type);
            service.send(logDto);
        });
    }

    public void stop() {
        es.shutdown();
    }

}
