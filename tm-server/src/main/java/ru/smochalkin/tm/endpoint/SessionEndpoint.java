package ru.smochalkin.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.api.endpoint.ISessionEndpoint;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.InetAddress;

@Component
@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @WebMethod
    @Override
    public SessionDto openSession(@WebParam(name = "login") @NotNull final String login,
                                  @WebParam(name = "password") @NotNull final String password) {
        return sessionService.open(login, password);
    }

    @WebMethod
    @Override
    public Result closeSession(@WebParam(name = "session") @NotNull final SessionDto sessionDto) {
        try {
            sessionService.close(sessionDto);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    @SneakyThrows
    public String getHost(){
        return InetAddress.getLocalHost().getHostName();
    }

}
