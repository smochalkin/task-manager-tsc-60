package ru.smochalkin.tm.util;

import org.jetbrains.annotations.Nullable;

public interface ValidateUtil {

    static boolean isEmpty(@Nullable final String value) {
        return value == null || value.isEmpty();
    }

}
