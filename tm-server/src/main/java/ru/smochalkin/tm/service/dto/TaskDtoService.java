package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.smochalkin.tm.api.repository.dto.ITaskDtoRepository;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;

import java.util.*;
import java.util.stream.Collectors;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

@Service
public class TaskDtoService extends AbstractDtoService<TaskDto> implements ITaskService {

    @NotNull
    public ITaskDtoRepository getRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        taskRepository.add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        taskRepository.clear();
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll() {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        return taskRepository.findAll();
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        return taskRepository.findById(id);
    }

    @Override
    @SneakyThrows
    public int getCount() {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        return taskRepository.getCount();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        taskRepository.clearByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<TaskDto> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findAll(@NotNull final String userId, @NotNull final String sort) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<TaskDto> comparator = sortType.getComparator();
        return taskRepository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        return taskRepository.findByIdAndUserId(userId, id);
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        return taskRepository.findByName(userId, name);
    }

    @Override
    @Nullable
    @SneakyThrows
    public TaskDto findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        taskRepository.removeById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        taskRepository.removeByIdAndUserId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        taskRepository.removeByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @Nullable final TaskDto project = findByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        taskRepository.removeByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @Nullable final TaskDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(desc);
        taskRepository.update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @Nullable final String desc
    ) {

        @Nullable final TaskDto entity = findByIndex(userId, index);
        if (entity == null) throw new EntityNotFoundException();
        entity.setName(name);
        entity.setDescription(desc);
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        taskRepository.update(entity);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String strStatus
    ) {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @Nullable final TaskDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        Status status = Status.getStatus(strStatus);
        Map<String, Date> dateMap = prepareDates(status);
        project.setStatus(status);
        project.setStartDate(dateMap.get("start_date"));
        project.setEndDate(dateMap.get("end_date"));
        taskRepository.update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String strStatus
    ) {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @Nullable final TaskDto project = findByName(userId, name);
        if (project == null) throw new EntityNotFoundException();
        Status status = Status.getStatus(strStatus);
        Map<String, Date> dateMap = prepareDates(status);
        project.setStatus(status);
        project.setStartDate(dateMap.get("start_date"));
        project.setEndDate(dateMap.get("end_date"));
        taskRepository.update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String strStatus
    ) {
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @Nullable final TaskDto project = findByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        Status status = Status.getStatus(strStatus);
        Map<String, Date> dateMap = prepareDates(status);
        project.setStatus(status);
        project.setStartDate(dateMap.get("start_date"));
        project.setEndDate(dateMap.get("end_date"));
        taskRepository.update(project);
    }

    @Override
    @SneakyThrows
    public boolean isNotIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) return true;
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        return index >= taskRepository.getCountByUser(userId);
    }

    @SneakyThrows
    private Map<String, Date> prepareDates(@NotNull final Status status) {
        Map<String, Date> map = new HashMap<>();
        switch (status) {
            case IN_PROGRESS:
                map.put("start_date", new Date());
                map.put("end_date", null);
                break;
            case COMPLETED:
                map.put("start_date", null);
                map.put("end_date", new Date());
                break;
            case NOT_STARTED:
                map.put("start_date", null);
                map.put("end_date", null);
        }
        return map;
    }

}
